### MLFlow Application Architecture

- Serverless MLFlow tracking server, hosted on AWS Fargate
- DB storage, hosted on Amazon RDS
- Artifact storage, hosted on Amazon S3

### Server Deployment

Before deploying the server, install and configure [the AWS CDK](https://docs.aws.amazon.com/cdk/v2/guide/getting_started.html) and [Docker](https://www.docker.com/).

The CDK stack details are described in `app.py`.

To install all dependencies and deploy the service into your AWS account, execute:

```
bash deploy_stack.sh
```

### Service Access

The tracking server is set up and running here: http://MLFlo-MLFLO-1GN8PLHKUUXL9-1220532633.eu-central-1.elb.amazonaws.com (only accessible through VPN).

